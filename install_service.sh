#!/bin/bash

export _PROGNAME="$( basename "$( readlink -f "$0" )" )"
export _DIRNAME="$( dirname "$( readlink -f "$0" )" )"

export _TEMPLATES_FOLDER="${_DIRNAME}/templates"
export _SYSTEMD_SERVICE_FOLDER=/etc/systemd/system
export _INIT_SERVICE_FOLDER=/etc/init.d

#[START] Errors
export _SUCCESS_STATUS=0  #[STATUS] Salida exitosa.
export _INVALID_ARGUMENT_ERROR=10  #[STATUS] Argumento invalido.
export _MISSING_SERVICE_NAME_ERROR=11  #[STATUS] Falto el nombre le servicio
export _SERVICE_ALREADY_INSTALLED_ERROR=12  #[STATUS] Servicio ya instalado.
export _NON_SUDO_USER_ERROR=12  #[STATUS] No se esta corriendo como superusuario.
#[END] Errors

#[START] Aliases
export _nul=/dev/null
alias dds="dd 2> $_nul"

shopt -s expand_aliases
#[ERROR] Aliases

log() {

    while [[ $# > 0 ]]
    do
        case "$1" in 
            -l | --level)
                shift
                declare -u _log_level="$1"
                shift
            ;;
            -n | --message_location )
                shift
                declare _message_location="$1"
                shift
            ;;
            -m | --message )
                shift
                declare _message="$1"
                shift
            ;;
            [^\^] )
                shift
                    [[ -z "${_message+x}" ]] || declare _message="$1"
                shift
            ;;
            * )
                shift
            ;;
        esac
    done
    
    declare -u _log_level="${_log_level:-"INFO"}"

    printf '%s|%s|%s:%s\n' "$_log_level" "$(date +'%Y/%m/%dT%H:%M:%S')" "$_message_location" "$_message"
}

usage() {
    
    declare _options_string="$( 
        egrep ".*#\[OPCION\].*" $_PROGNAME \
        | sed -r 's/(.*)(\).*)( #.*)/\1:\3/g' \
        | tr "|" "," \
        | tr -s " " \
        | column -t -s ":"
    )"
    
    declare _status_string="$( 
        egrep ".*#\[STATUS\].*" $_PROGNAME \
        | egrep -o "([[:digit:]]+[[:space:]]+#.*)" \
        | sed -r 's/(.*)(#.*)/ \1:\2/g' \
        | tr -s " " \
        | column -t -s ":"
    )"
    
    declare _usage_string="Usage: %s [OPCION] [NOMBRE]:\n%s\n\nExit status:\n%s\n"

    printf "$_usage_string" "${_PROGNAME}" "${_options_string}" "${_status_string}"
}

invalid_argument() {

    declare _invalid_argument="$1"
    echo "Opcion: \"$_invalid_argument\" invalida."
    usage
    exit $_INVALID_ARGUMENT_ERROR
}

check_service() {
    
    declare current_user="$(id -un)"

    [[ "$current_user" != "root" ]] \
        && log \
            --level WARNING \
            --message_location ${FUNCNAME} \
            --message "Es de preferencia que se ejecute con permisos superusuario para acceder a todas las caracteristicas."

    [[ -z "${_only_generate+x}" && "$current_user" != "root" ]] \
        && log \
            --level ERROR \
            --message_location ${FUNCNAME} \
            --message "No se esta corriendo con permisos de superusuario." \
        && exit $_NON_SUDO_USER_ERROR

    [[ -z "${_SERVICE_NAME}" ]] \
        && log \
            --level ERROR \
            --message_location ${FUNCNAME} \
            --message "Falto el nombre del servicio. Correr: \"$_PROGNAME -h\" para ver opciones disponibles." \
        && exit $_MISSING_SERVICE_NAME_ERROR

    [[ -e "${_SYSTEMD_SERVICE_FOLDER}/${_SERVICE_NAME}" ]] \
        && log \
            --level ERROR\
            --message_location ${FUNCNAME} \
            --message "El servicio \"${_SERVICE_NAME}\" ya se encuentra instalado." \
        && exit $_SERVICE_ALREADY_INSTALLED_ERROR
}


fill_templates() {
    declare _SERVICE_GENERATED_LOCATION="$_DIRNAME/gen"

    if [[ ! -d "$_SERVICE_GENERATED_LOCATION" ]]
    then
        log \
            --level INFO \
            --message_location ${FUNCNAME} \
            --message "Creando el directorio: $_SERVICE_GENERATED_LOCATION"
        mkdir "$_SERVICE_GENERATED_LOCATION"
    fi

    sed "s/{service}/${_SERVICE_NAME}/g" < "${_TEMPLATES_FOLDER}/service_script_template" \
            | dds of="$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}"
    sed -e "s:{service_location}:${_INIT_SERVICE_FOLDER}/${_SERVICE_NAME}:g" \
        -e "s/{service}/${_SERVICE_NAME}/g" < "${_TEMPLATES_FOLDER}/service_template" \
        | dds of="$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}.service"

    chmod 744 "$_SERVICE_GENERATED_LOCATION/$_SERVICE_NAME"
    chmod 644 "$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}.service"

    log \
        --message_location $FUNCNAME \
        --message "Editando archivo ${_SERVICE_NAME}."
    vim "$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}"

    log \
        --message_location $FUNCNAME \
        --message "Editando archivo ${_SERVICE_NAME}."
    vim "$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}.service"

    if [[ -z "${_only_generate+x}" ]]
    then

        log \
            --level INFO \
            --message_location $FUNCNAME \
            --message "Copiando archivos a su localizacion objetivo."
        
        cp "$_SERVICE_GENERATED_LOCATION/${_SERVICE_NAME}.service" "$_SYSTEMD_SERVICE_FOLDER"
        if [[ $? -eq 0 ]]
        then
            log \
                --level INFO \
                --message_location $FUNCNAME \
                --message "Archivos ${_SERVICE_NAME}.service copiado $_SYSTEMD_SERVICE_FOLDER."
        else
            log \
                --level ERROR \
                --message_location $FUNCNAME \
                --message "Problema al copiar ${_SERVICE_NAME}.service a $_SYSTEMD_SERVICE_FOLDER."
        fi

        cp "$_SERVICE_GENERATED_LOCATION/$_SERVICE_NAME" "$_INIT_SERVICE_FOLDER"
        if [[ $? -eq 0 ]]
        then
            log \
                --level INFO \
                --message_location $FUNCNAME \
                --message "Archivos ${_SERVICE_NAME}.service copiado $_INIT_SERVICE_FOLDER."
        else
            log \
                --level ERROR \
                --message_location $FUNCNAME \
                --message "Problema al copiar $_SERVICE_NAME a $_INIT_SERVICE_FOLDER."
        fi
    fi
}

enable_service() {

    log \
        --message_location $FUNCNAME \
        --message "Habilitando el sevicio $_SERVICE_NAME"
    
    systemctl enable "$_SERVICE_NAME"
    if [[ $? -eq 0 ]]
    then
        log \
            --message_location $FUNCNAME \
            --message "Servicio habilitado."
    else
        log \
            --level ERROR \
            --message_location $FUNCNAME \
            --message "Error al habilitar."

        declare title="Menu"
        declare prompt="Elige una opcion a ejecutar: "
        declare -a options=("daemon-reload")

        echo "$title"
        declare PS3="$prompt "
        select opt in "${options[@]}" "Continuar con el programa"
        do
            case "$REPLY" in
                1 )
                    systemctl daemon-reload
                    systemctl enable "$_SERVICE_NAME"
                    [[ $? -eq 0 ]] \
                        && log \
                            --message_location $FUNCNAME \
                            --message "Servicio habilitado." \
                        || log \
                            --level ERROR \
                            --message_location $FUNCNAME \
                            --message "Error al volver a habilitar."
                ;;

                $(( ${#options[@]}+1 )) ) 
                    echo "Continuando"
                    break
                ;;
                *) 
                    echo "Opcion \"$REPLY\" invalida. Intentar de nuevo."
                    continue
                ;;

            esac

        done
    fi
}

main() {

    while [[ $# > 0 ]]
    do
        case "$1" in
            --servicio | -s )  #[OPCION] Nombre del servicio.
                shift
                if [[ ! "$1" =~ (--only-generate|-h|--help|-s|--servicio) ]] 
                then
                    export _SERVICE_NAME="$1"
                else 
                    log \
                    --level ERROR \
                    --message_location ${FUNCNAME} \
                    --message "Falto el nombre del servicio. Correr: \"$_PROGNAME -h\" para ver opciones disponibles."
                    
                    exit $_MISSING_SERVICE_NAME_ERROR
                fi
                shift
            ;;
            
            -h | --help )  #[OPCION] Ayuda.
                usage
                exit $_SUCCESS_STATUS
            ;;
            --only-generate )  #[OPCION] Unicamente genera los archivos en el directorio "gen".
                shift
                export _only_generate=0
                shift
            ;;

            * )
                invalid_argument "$1"
            ;;
        esac
    done

    check_service
    fill_templates
    enable_service
}

main $@
